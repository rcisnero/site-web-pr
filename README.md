# Projet PR - PR00
Site web d'administration d'équipements sportifs.

[![PHP](https://img.shields.io/badge/-PHP-success.svg)](https://gitlab.utc.fr/rcisnero/site-web-pr)
[![SQL](https://img.shields.io/badge/-SQL-important.svg)](https://gitlab.utc.fr/rcisnero/site-web-pr)
[![HTML](https://img.shields.io/badge/-HTML-critical.svg)](https://gitlab.utc.fr/rcisnero/site-web-pr)
[![CSS](https://img.shields.io/badge/-CSS-informational.svg)](https://gitlab.utc.fr/rcisnero/site-web-pr)
